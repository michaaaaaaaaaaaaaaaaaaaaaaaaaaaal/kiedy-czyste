const express = require('express')
const app = express()
const PORT = process.env.PORT || 2137
const cors = require('cors')

let values = []

app.use(cors())
app.use(express.json())

app.get('/value/:number', (req, res) => {
  const { number } = req.params

  values.push(parseInt(number))

  if (values.length > 5000) {
    values.shift()
  }

  res.status(200).send('Value added successfully')
})

app.get('/values', (req, res) => {
  res.status(200).json(values)
})

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})

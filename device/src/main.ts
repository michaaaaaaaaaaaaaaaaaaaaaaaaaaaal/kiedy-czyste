import * as ds from "@devicescript/core"
import { I2CSensorDriver } from "@devicescript/drivers"
import { i2c } from "@devicescript/i2c"
import { fetch } from "@devicescript/net"
import { debounceTime } from "@devicescript/observables"
import { mcuTemperature, setStatusLight } from '@devicescript/runtime'
import { configureHardware } from "@devicescript/servers"

// https://microsoft.github.io/devicescript/devices/esp32/esp32-bare
import { pins, board } from "@dsboard/esp32_bare"

configureHardware({
    i2c: {
        pinSDA: pins.P21,
        pinSCL: pins.P22,
    },
    scanI2C: true
})

const NETWORKS: [string, string][] = [
    // ["jak tam chlopie", "dupacyce"], // nie ja ustawialem haslo
    ["MichaliPhone", "fenomenalnie"],
    // ["K2Media950", "a47e398b"],
    // ["K2Media950-5G", "a47e398b"]
]

const logger = (scope: string) => (a: any, b?: any) => {
    // Spread operator is not supported
    console.log(`[${scope}]`, a, b ?? '')
}

/**
 * Loop
 */

const logValue = async (value: string) => {
    try {
        await fetch(`http://kcapi.jabczyk.com/value/` + value, {
            method: "GET"
        })
    } catch (err) {
        console.error('[loop] failed to log value:', err)
    }
}

// const POWER_DOWN = 0x00
// const POWER_ON = 0x01
// const RESET = 0x07
// const CONTINUOUS_H_RESOLUTION_MODE = 0x10
// const CONTINUOUS_H_RESOLUTION_MODE2 = 0x11
// const CONTINUOUS_L_RESOLUTION_MODE = 0x13
// const ONETIME_H_RESOLUTION_MODE = 0x20
// const ONETIME_H_RESOLUTION_MODE2 = 0x21
// const ONETIME_L_RESOLUTION_MODE = 0x23
// const ADDRESS = 0x1
const ADDRESS = 0x23
const READ_THROTTLE = 0

class BH1750Driver extends I2CSensorDriver<{
    light: string
}> {
    log: any

    constructor() {
        super(ADDRESS, { readCacheTime: READ_THROTTLE })
        this.log = logger('BH1750')
    }

    override async initDriver(): Promise<void> {
        this.log('inicjacja drivera...')

        const BH1750_ONE_TIME_HIGH_RES_MODE = hex`20`
        // try {
        await this.writeBuf(BH1750_ONE_TIME_HIGH_RES_MODE)
            // const buf = await this.readBuf(1)
            // console.log(buf)
        // } catch (err) {
        //     this.log('inicjacja drivera nie powiodla sie')
        // }

        this.log('inicjacja zakonczona')
        await ds.delay(100)
    }

    override async readData() {
        this.log('odczytywanie...')

        const buf = await this.readBuf(2) // czyta z adresu ADDRESS
        this.log('dostalismy bufa')

        const light = buf.readUInt16BE(0) / 1.2

        this.log('odczytywanie zakonczone')

        return { light }
    }
}

const lightSensor = new BH1750Driver()
await lightSensor.init()

const LOOP_INTERVAL = 5000

const loop = async () => {
    const log = logger('loop')
    log("loop...")

    const sensor = await lightSensor.read()
    console.log('sensor data' + sensor.light)

    await logValue(sensor.light)
    // await setStatusLight(1)
}

/**
 * Setup
 */

const enableWifiIfNotEnabled = async (wifi: ds.Wifi) => {
    const log = logger('wifi')
    const isEnabled = await wifi.enabled.read()
    if (!isEnabled) {
        await wifi.enabled.write(true)
    }
    log('wifi enabled')
}

const listenToWifiEvents = (wifi: ds.Wifi) => {
    const log = logger('wifi')

    const debounce = debounceTime(1000)

    wifi.ssid.pipe(debounce).subscribe(ssid => {
        if (ssid === '') {
            log('connected: [no]')
            return
        }
        log('connected:', ssid)
    })

    wifi.gotIp.pipe(debounce).subscribe(() => {
        log('ip: [got]')
    })

    wifi.lostIp.pipe(debounce).subscribe(() => {
        log('ip: [lost]')
    })

    wifi.connectionFailed.pipe(debounce).subscribe(ctx => {
        log('connection failed :(', ctx)
    })

    // wifi.eui48.subscribe(mac => {
    //     log('mac:', mac.toString('hex'))
    // })

    wifi.ipAddress.subscribe(ip => {
        log('ip addr:', ip.toString('hex'))
    })

    wifi.networksChanged.pipe(debounce).subscribe(() => {
        log('known networks changed')
    })

    wifi.scanComplete.pipe(debounce).subscribe(() => {
        log('scan complete')
    })

}

const wifiScan = async (wifi: ds.Wifi) => {
    const log = logger('wifi')
    await wifi.scan()
    log('scanning for networks...')

    // await wifi.scanComplete.wait()
    await ds.delay(10_000)
    log('scan possibly completed')
}

const setupWifi = async () => {
    const log = logger('wifi')

    const wifi = new ds.Wifi()
    log('setting up...')

    listenToWifiEvents(wifi)
    await enableWifiIfNotEnabled(wifi)
    await wifiScan(wifi)

    for (let i = 0; i < NETWORKS.length; i++) {
        const [ssid, password] = NETWORKS[i]
        await wifi.addNetwork(ssid, password)
        await wifi.setNetworkPriority(10 - i, ssid)
        log('network added:', ssid)
    }

    await wifiScan(wifi)
}

const setup = async () => {
    console.log('setting up...')
    await setupWifi()
    console.log('setup complete')
    setInterval(loop, LOOP_INTERVAL)
}
setTimeout(setup, 1000)

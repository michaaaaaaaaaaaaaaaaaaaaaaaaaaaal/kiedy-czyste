# - project name -

This project uses [DeviceScript](https://microsoft.github.io/devicescript/).

https://www.pololu.com/docs/0J7/all#2

# Ustaw board VMki

npx devicescript flash --board esp32_bare
npx devicescript flash --board esp32_az

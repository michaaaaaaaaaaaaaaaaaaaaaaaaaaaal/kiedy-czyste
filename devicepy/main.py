import esp32
import network
from machine import Pin, I2C, SoftI2C
import urequests

import bh1750
from sensor_pack.bus_service import I2cAdapter
import time

if __name__ == '__main__':
    SSID = "MichaliPhone"
    PASSWORD = "fenomenalnie"
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(SSID, PASSWORD)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())

    SCL_PIN = Pin(22)
    SDA_PIN = Pin(21)
    i2c = SoftI2C(scl=SCL_PIN, sda=SDA_PIN, freq=400_000)
    adaptor = I2cAdapter(i2c)
    sol = bh1750.Bh1750(adaptor, 0x23) # 0x23 by default, 0x5C with address

    sol.power(on=True)
    sol.set_mode(continuously=True, high_resolution=True)
    sol.measurement_accuracy = 1.0
    old_lux = curr_max = 1.0

    for lux in sol:
        if lux != old_lux:
            print(f"lux: {lux}")
        old_lux = lux

        response = urequests.get("http://65.21.137.169:2137/value/" + str(int(lux)))
        response.close()

        # time.sleep_ms(10000)
        time.sleep_ms(5000)

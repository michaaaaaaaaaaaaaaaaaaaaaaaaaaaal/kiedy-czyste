import Head from 'components/Head'
import Timer from 'components/Timer'
import { getElapsed } from 'getElapsed'
import { useEffect, useState, type ReactElement } from 'react'
import { tw } from 'utils'

const MIN = 60
const HR = 60 * MIN
const DURATIONS = [
	30 * MIN,
	60 * MIN,
	90 * MIN,
	2 * HR + 40 * MIN
]

export default function Homepage(): ReactElement {
	const [duration, setDuration] = useState(DURATIONS[2])
	const [elapsed, setElapsed] = useState(0)

	useEffect(() => {
		setInterval(() => {
			getElapsed().then(v => setElapsed(v)).catch(console.error)
		}, 5000)
	}, [])

	return (
		<>
			<Head title='Kiedy czyste?' />

			<div className='flex w-screen h-screen'>
				<div className='w-full h-full m-auto p-5 border border-gray-500 max-w-md lg:max-h-[700px]'>
					<Timer duration={duration} elapsed={elapsed} />

					<div className='flex justify-center space-x-4'>
						{DURATIONS.map((d) => {
							const isActive = d === duration

							return (
								<button
									type='button'
									key={d}
									className={tw('py-2 px-4 w-16 h-16 text-white rounded-full border-2', isActive ? 'bg-emerald-700' : 'bg-emerald-700/60')}
									onClick={() => setDuration(d)}
								>
									<div className='font-bold'>{d / MIN}</div>
									<small>min</small>
								</button>
							)
						})}
				</div>
			</div>
			</div>
		</>
	)
}

import type { ReactElement } from 'react'

interface Properties {
	error?: Error
}
export default function LoadingOrError({ error }: Properties): ReactElement {
	return (
		<div className='flex w-screen h-screen items-center justify-center'>
			<h1 className='text-xl' data-testid='LoadingOrError'>
				{error ? error.message : 'Ładowanie...'}
			</h1>
		</div>
	)
}
LoadingOrError.defaultProps = {
	error: undefined
}

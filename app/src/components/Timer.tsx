import type React from 'react';
import { CircularProgressbarWithChildren, buildStyles } from 'react-circular-progressbar';

interface Properties {
  duration: number
  elapsed: number
}

const getElapsedText = (elapsed: number, duration: number): string => {
  const left = duration - elapsed
  const min = Math.floor(left / 60)
  // const sec = left % 60

  return `${min} min.`


}

const Timer: React.FC<Properties> = ({ duration, elapsed }) => {
  console.log('re')

  return (
    <div style={{ width: 300, height: 300 }} className='mx-auto mb-10 mt-20'>
      <CircularProgressbarWithChildren
       maxValue={duration}
       value={elapsed}
      //  text={getElapsedText(elapsed, duration)}
      styles={buildStyles({
        // Rotation of path and trail, in number of turns (0-1)
        // rotation: 0.25,

        // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
        strokeLinecap: 'butt',

        // Text size
        textSize: 30,

        // How long animation takes to go from one percentage to another, in seconds
        pathTransitionDuration: 0.5,

        // Can specify path transition in more detail, or remove it entirely
        // pathTransition: 'none',

        // Colors
        pathColor: `#85ad9c`,
        textColor: '#347659',
        trailColor: '#eee',
        backgroundColor: '#3e98c7',
      })}
      >
        <div className='text-5xl font-bold'>
          {getElapsedText(elapsed, duration)}
        </div>

        <div className='mt-2'>
          {elapsed === 0 ? 'WYŁĄCZONA' : 'ZMYWANIE...'}
        </div>
      </CircularProgressbarWithChildren>
    </div>
  )
}

export default Timer
const calculateBaseline = (values: number[]): number => {
  // input is unsorted array
  // get the average of bottom 10% of values
  const sorted = values.sort((a, b) => a - b);
  const tenPercent = Math.floor(values.length / 10);
  const bottomTenPercent = sorted.slice(0, tenPercent);
  const sum = bottomTenPercent.reduce((accumulator, value) => accumulator + value, 0);
  return sum / bottomTenPercent.length;
}

const calculateLightUpFor = (values: number[], baseline: number, lightUpDiff: number): number => {
  // given an array of values, from newest to oldest, return the count of
  // consecutive values that are higher from the baseline by at least lightUpDiff
  // units
  let count = 0;
  for (let index = 0; index < values.length; index++) {
    const value = values[index];
    if (value > baseline + lightUpDiff) {
      count += 1;
    } else {
      break;
    }
  }

  return count;
}

export const getElapsed = async (): Promise<number> => {
  const response = await fetch('https://kcapi.jabczyk.com/values');
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  const valuesOld: number[] = await response.json();
  const values: number[] = []
  for (let index = valuesOld.length - 1; index !== 0; index -= 1) {
    values.push(valuesOld[index]);
  }

  const lightUpDiff = 200;
  const secsPerMeasurement = 5;
  const baseline = calculateBaseline([...values]);
  const lightUpForMeasurements = calculateLightUpFor([...values], baseline, lightUpDiff);
  const elapsed = lightUpForMeasurements * secsPerMeasurement;
  console.log({ lightUpDiff, secsPerMeasurement, baseline, lightUpForMeasurements, elapsed, values })


  return elapsed;
}
